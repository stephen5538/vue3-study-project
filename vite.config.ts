/**
 * 这个文件使用来配置 代理跨域 问题的
 */

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
//引入svg需要用到的插件
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
//mock数据
import { viteMockServe } from 'vite-plugin-mock'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  //加载.env.development各文件的变量
  //process.cwd() -> 项目根目录（index.html 文件所在的位置）。
  //可以是一个绝对路径，或者一个相对于该配置文件本身的相对路径。
  const env = loadEnv(mode, process.cwd())
  return {
    plugins: [
      vue(),
      //配置mock插件
      viteMockServe({
        localEnabled: command === 'serve',
      }),
      //配置svg插件
      createSvgIconsPlugin({
        //将来的svg小图标放入src/assets/icons目录
        iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
        symbolId: 'icon-[dir]-[name]',
      }),
    ],
    //配置src路径 使用@来替代src路径
    resolve: {
      alias: {
        '@': path.resolve('./src'), // 相对路径别名配置
      },
    },
    //配置了 CSS 预处理器选项
    //"./src/styles/variable.scss"，这样所有 SCSS 文件都会自动导入该文件中的变量。
    css: {
      preprocessorOptions: {
        scss: {
          javascriptEnabled: true,
          additionalData: '@import "./src/styles/variable.scss";',
        },
      },
    },
    //配置代理跨域
    server: {
      proxy: {
        // '/api': {
        [env.VITE_APP_BASE_API]: {
          //目标服务器
          target: env.VITE_SERVE,
          //是都需要跨域
          changeOrigin: true,
          //路径重写
          // rewrite: (path) => path.replace(/^\/api/, ''),
          rewrite: (path) => path.replace(env.VITE_APP_BASE_API, ''),
        },
        [env.VITE_APP_USER_BASE_API]: {
          //目标服务器
          target: env.VITE_USER_SERVE,
          //是都需要跨域
          changeOrigin: true,
          //路径重写
          // rewrite: (path) => path.replace(/^\/api/, ''),
          rewrite: (path) => path.replace(env.VITE_APP_USER_BASE_API, ''),
        },
      },
    },
  }
})
