//创建用户相关的小仓库
import { defineStore } from 'pinia'

const useLayOutSettingStore = defineStore('SettingStore', {
  state: () => {
    return {
      //用于控制菜单收起/折叠
      fold: false,
      //顶部刷新按钮
      refresh: false,
    }
  },
})

export default useLayOutSettingStore
