//创建用户相关的小仓库
import { defineStore } from 'pinia'
//引入请求方法
import { reqLogin, reqUserInfo, reqLogout } from '@/api/user'
//引入数据类型
import type { LoginForm, UserInfo } from '@/api/user/type'
import type { ResponseData } from '@/api/type'

import type { UserState } from './types/type'
import { GET_TOKEN, SET_TOKEN, REMOVE_TOKEN } from '@/utils/token'
import { constRoute, asyncRoute, anyRoute } from '@/router/routes'
//引入深拷贝方法
//@ts-expect-error
import cloneDeep from 'lodash/cloneDeep'
import router from '@/router'

//用于过滤当前用户需要展示的异步路由
function filterAsyncRoute(asnycRoute: any[], routes: any[]) {
  return asnycRoute.filter((item: any) => {
    if (routes.includes(item.name)) {
      if (item.children && item.children.length > 0) {
        item.children = filterAsyncRoute(item.children, routes)
      }
      return true
    }
  })
}

const useUserStore = defineStore('User', {
  //小仓库存储数据地方
  state: (): UserState => {
    return {
      //用户信息
      userInfo: {
        username: '',
        avatar: '',
      },
      //token
      //   token: localStorage.getItem('token') || '',
      token: GET_TOKEN(),
      //菜单权限
      menuRoutes: constRoute,
    }
  },
  //异步|逻辑的地方
  actions: {
    /**
     * 用户登录函数
     *
     * @param data 用户登录信息，预期格式为LoginForm
     * @returns 返回Promise，解析为字符串'ok'表示登录成功，否则为拒绝的Promise
     */
    async userLogin(data: LoginForm) {
      // 发起登录请求
      const result: ResponseData<string> = await reqLogin(data)
      // 判断请求是否成功
      if (result.code === 200) {
        // 登录成功
        if (result.data) {
          // 保存token
          this.token = result.data
          // 将token保存到本地
          SET_TOKEN(result.data)
          // localStorage.setItem('token', result.data.token)
          // 返回登录成功信息
          return 'ok'
        }
        // token为空时，拒绝Promise
        return Promise.reject(new Error('token值为空'))
      } else {
        // 其他错误情况，拒绝Promise并附带错误信息
        return Promise.reject(new Error(result.message))
      }
    },
    /**
     * 异步请求用户信息
     *
     * 本函数通过发送异步请求获取用户信息，并更新当前实例的用户信息属性
     * 请求成功时，会从响应数据中提取用户名和头像，并将其赋值给实例的userInfo属性
     */
    async reqUserInfo() {
      // 发送异步请求获取用户信息，并等待响应
      const result: ResponseData<UserInfo> = await reqUserInfo()
      // 检查响应状态码，如果为200，则更新用户信息
      if (result.code === 200 && result.data) {
        // 从响应数据中提取并更新用户名和头像
        this.userInfo.username = result.data.name
        this.userInfo.avatar = result.data.avatar
        this.buttons = result.data.buttons
        console.log(result.data)
        //计算当前用户需要展示的异步路由
        const userAsyncRoute = filterAsyncRoute(
          cloneDeep(asyncRoute),
          result.data.routes,
        )
        console.log(userAsyncRoute)
        //菜单需要的数据整理完毕
        this.menuRoutes = [...constRoute, ...userAsyncRoute, anyRoute]
        //目前路由器管理的只有常量路由:用户计算完毕异步路由、任意路由动态追加
        ;[...userAsyncRoute, anyRoute].forEach((route: any) => {
          router.addRoute(route)
        })

        return 'ok'
      } else {
        return Promise.reject(new Error(result.message))
      }
    },
    /**
     * 该方法用于用户登出
     * 它通过清除令牌和用户信息来实现登出功能
     */
    async logout() {
      const res = await reqLogout()
      if (res.code == 200) {
        // 清除令牌
        this.token = ''
        // 将用户信息重置为空对象
        this.userInfo = {
          username: '',
          avatar: '',
        }
        // 调用REMOVE_TOKEN函数，清除存储的令牌
        REMOVE_TOKEN()
        return 'ok'
      } else {
        return Promise.reject(new Error(res.message || 'Error'))
      }
    },
  },
  getters: {},
})

export default useUserStore
