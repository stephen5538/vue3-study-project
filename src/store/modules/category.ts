import { defineStore } from 'pinia'
import {
  reqGetCategory1,
  reqGetCategory2,
  reqGetCategory3,
} from '@/api/product/category'
import { Categories } from '@/api/product/category/type'
import { ResponseData } from '@/api/type'
import { CategoryState } from './types/type'

const useCategoryStore = defineStore('CategoryStore', {
  state: (): CategoryState => {
    return {
      category1List: [],
      category1Id: '',
      category2List: [],
      category2Id: '',
      category3List: [],
      category3Id: '',
    }
  },

  actions: {
    async getCategory1List() {
      const res: ResponseData<Categories> = await reqGetCategory1()
      if (res.code == 200) {
        this.category1List = res.data
      }
    },
    async getCategory2List() {
      if (this.category1Id) {
        const res: ResponseData<Categories> = await reqGetCategory2(
          this.category1Id,
        )
        if (res.code == 200) {
          this.category2List = res.data
        }
      }
    },
    async getCategory3List() {
      if (this.category2Id) {
        const res: ResponseData<Categories> = await reqGetCategory3(
          this.category2Id,
        )
        if (res.code == 200) {
          this.category3List = res.data
        }
      }
    },
  },
  getters: {},
})

export default useCategoryStore
