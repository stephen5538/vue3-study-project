import type { RouteRecordRaw } from 'vue-router'
import { Categories } from '@/api/product/category/type'
//定义小仓库state数据类型
export interface UserState {
  token: string | null
  userInfo: User
  menuRoutes: RouteRecordRaw[]
  buttons?: string[]
}

interface User {
  username: string
  avatar: string
}

export interface CategoryState {
  category1List: Categories
  category1Id: number | string
  category2List: Categories
  category2Id: number | string
  category3List: Categories
  category3Id: number | string
}
