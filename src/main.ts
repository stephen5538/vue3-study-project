import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

//这个函数的功能是导入SVG图标注册模块。通过引入virtual:svg-icons-register，
//可以将SVG图标文件注册到项目中，以便在应用中使用这些图标。
//这个步骤通常是必要的，以便能够通过组件或其它方式引用和渲染SVG图标
import 'virtual:svg-icons-register'
// import SvgIcon from '@/components/SvgIcon/index.vue'
//引入所有components目录下的自定义组件
import globalComponent from '@/components/index'
import pinia from '@/store'
import { createApp } from 'vue'
import App from '@/App.vue'
//引入路由
import router from './router'
//引入自定义全局样式
import '@/styles/index.scss'

const app = createApp(App)
//安装仓库
app.use(pinia)

//全局安装elementPlus
app.use(ElementPlus, {
  //使用中文
  locale: zhCn,
})
/**
 * 单个引入组件太麻烦 使用全局安装app.use(globalComponent)
 */
//全局挂载svg自定义组件
// app.component('SvgIcon', SvgIcon)
//全局安装所有自定义组件
app.use(globalComponent)
//注册路由
import './permission'
app.use(router)
app.mount('#app')
document.title = import.meta.env.VITE_APP_TITLE
//打印环境变量
console.log('打印环境变量', import.meta.env)

//暗黑模式
import 'element-plus/theme-chalk/dark/css-vars.css'

//引入自定义指令文件
import { isHasButton } from '@/directive/has'
isHasButton(app)
