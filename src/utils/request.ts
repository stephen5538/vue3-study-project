//进行axios的二次封装 ，使用请求和响应拦截器
import useUserStore from '@/store/modules/user'
import axios from 'axios'
import { ElMessage } from 'element-plus'

//使用create创建axios实例
const request = axios.create({
  //基础路径
  baseURL: import.meta.env.VITE_APP_BASE_API,
  //超时时间
  timeout: 5000,
})

//添加请求响应拦截器
request.interceptors.request.use((config) => {
  const userStore = useUserStore()
  //headers属性请求头
  if (userStore.token) {
    config.headers.token = userStore.token
  }
  return config
})

//添加响应拦截器
request.interceptors.response.use(
  //成功放回
  (response) => {
    return response.data
  },
  //网络错误
  (error) => {
    let message = ''
    if (error.response) {
      let status = error.response.status
      switch (status) {
        case 401:
          message = 'token过期'
          break
        case 403:
          message = '无权访问'
          break
        case 404:
          message = '请求地址错误'
          break
        case 500:
          message = '服务器出现问题'
          break
        default:
          message = '网络出现问题'
          break
      }
    } else {
      message = '网络出现问题'
    }
    ElMessage.error({
      type: 'error',
      message,
    })
    return Promise.reject(error)
  },
)

export default request
