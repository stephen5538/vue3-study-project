import router from '@/router'
//引入页面加载进度条
import nProgress from 'nprogress'
//加载进度条的样式
import 'nprogress/nprogress.css'
import pinia from '@/store'
import useUserStore from '@/store/modules/user'

const userStore = useUserStore(pinia)
//去掉加载的小圈
nProgress.configure({ showSpinner: false })
function listRoutes(routes: any[]) {
  return routes.reduce((pre, cur) => {
    // 递归调用
    pre.push(cur)
    if (cur.children && cur.children.length > 0) {
      pre = pre.concat(listRoutes(cur.children))
    }
    return pre
  }, [])
}
//全局前置守卫
router.beforeEach((to: any, _from: any, next: any) => {
  //修改title
  document.title = `${import.meta.env.VITE_APP_TITLE} | ${to.meta.title}`
  nProgress.start()
  const token = userStore.token
  const username = userStore.userInfo?.username

  if (token) {
    //登陆后不能访问登陆页面
    if (to.path === '/login') {
      next('/')
    } else {
      //有了token且有了用户信息才能放行
      if (username) {
        next()
      } else {
        //发请求获取用户信息
        userStore
          .reqUserInfo()
          .then(() => {
            //万一:刷新的时候是异步路由,有可能获取到用户信息、异步路由还没有加载完毕,出现空白的效果
            const paths = listRoutes(userStore.menuRoutes).map(
              (ifr) => ifr.path,
            )

            if (paths.includes(to.path)) {
              next({ ...to })
            } else {
              next({ path: '/404' })
            }
          })
          .catch(() => {
            //token失效，清除token
            userStore.logout().then(() => {
              //回到登录页
              next({ path: '/login', query: { redirect: to.path } })
            })
          })
      }
    }
    //未登录，直接回到登录页
  } else {
    if (to.path === '/login') {
      next()
    } else {
      //带着跳转地址回到登录页
      next({ path: '/login', query: { redirect: to.path } })
    }
  }
})

//全局后置守卫
router.afterEach((_to: any, _from: any) => {
  nProgress.done()
})
