/**
 * 用于注册全局组件
 * 该函数通过插件方式将多个全局组件注册到 Vue 应用中，使得这些组件可以在应用的任何地方被使用
 */

import SvgIcon from '@/components/SvgIcon/index.vue'
import Pagination from '@/components/Pagination/index.vue'
import Category from '@/components/Category/index.vue'
//注册全局element-plus icon
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import type { App, Component } from 'vue'
// allGlobalComponents是一个对象，其中的键是字符串类型，值是 Component 类型
const allGlobalComponents: { [name: string]: Component } = {
  SvgIcon,
  Pagination,
  Category,
}

//对外暴露插件对象
export default {
  //插件对象必须提供install方法
  install(app: App) {
    //在 install 方法中，通过遍历 allGlobalComponents 对象的键，
    //将每个全局组件注册到 Vue 应用中，
    //其中 key 作为组件名，allGlobalComponents[key] 作为组件本身
    Object.keys(allGlobalComponents).forEach((key) => {
      app.component(key, allGlobalComponents[key])
    })
    //将elementplus提供的图标注册到全局
    for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
      app.component(key, component)
    }
  },
}
