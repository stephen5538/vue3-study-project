//用户登录请求参数
export interface LoginForm {
  username: string
  password: string
}

export interface UserInfo {
  avatar: string
  name: string
  roles: string[]
  buttons: string[]
  routes: string[]
}
interface CheckUser {
  checkUser: UserInfo
}

export interface UserInfoResponseData {
  code: number
  data: CheckUser
}
