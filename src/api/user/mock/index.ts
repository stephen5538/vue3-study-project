//统一管理用户相关接口
import request from '@/utils/request'
import type { LoginForm, LoginResponseData, UserInfoResponseData } from './type'

enum API {
  LOGIN_URL = '/user/login',
  USERINFO_URL = '/user/info',
}

//暴漏请求函数
/**
 * 用户登录请求
 *
 * 该函数封装了向后端请求用户登录的POST请求它将用户登录数据作为参数发送到指定的登录URL
 * 主要用于处理用户登录流程，通过网络请求实现与后端服务器的交互
 *
 * @param data 任意类型的数据参数，通常应包含用户名和密码等登录信息
 * @returns 返回请求的结果，通常是一个Promise对象，包含登录操作的结果或错误信息
 */
export const reqLogin = (data: LoginForm) =>
  request.post<any, LoginResponseData>(API.LOGIN_URL, data)
/**
 * 请求用户信息
 *
 * 本函数通过发送GET请求到用户信息接口，获取当前用户的详细信息
 * 不需要传递任何参数，直接调用即可
 *
 * @returns Promise 返回一个Promise对象，包含用户信息的响应数据
 */
export const reqUserInfo = () =>
  request.get<any, UserInfoResponseData>(API.USERINFO_URL)
