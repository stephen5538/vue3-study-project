import request from '@/utils/userRequest'
import type { ResponseData, PageRecord } from '@/api/type'
import type { User } from './type'

enum API {
  PAGE_USER = '/admin/acl/user',
  ADD_USER = '/admin/acl/user/save',
  UPDATE_USER = '/admin/acl/user/update',
  REMOVE_USER = '/admin/acl/user/remove',
  BATCH_REMOVE_USER = '/admin/acl/user/batchRemove',
}

export const reqUserList = (
  page: number,
  limit: number,
  username: string,
  name: string,
) =>
  request.get<null, ResponseData<PageRecord<User>>>(
    API.PAGE_USER + `/${page}/${limit}?username=${username}&name=${name}`,
  )

export const reqAddOrUpdateUser = (data: User) => {
  if (data.id) {
    return request.put<User, ResponseData<string>>(API.UPDATE_USER, data)
  } else {
    return request.post<User, ResponseData<string>>(API.ADD_USER, data)
  }
}

export const reqRemoveUser = (id: number | string) =>
  request.delete<null, ResponseData<string>>(API.REMOVE_USER + `/${id}`)

export const reqBatchRemoveUser = (idList: number[]) =>
  request.delete<null, ResponseData<string>>(API.BATCH_REMOVE_USER, {
    data: idList,
  })
