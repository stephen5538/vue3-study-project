export interface User {
  id?: number | string
  username: string
  password: string
  name: string
  phone?: string
  roleName?: string
  createTime?: string
  updateTime?: string
}

export type Users = User[]
