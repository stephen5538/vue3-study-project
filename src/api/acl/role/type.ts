export interface Role {
  id?: number | string
  roleName: string
  remark: string
}

export type Roles = Role[]

export interface RoleAssign {
  allRolesList: Roles
  assignRoles: Roles
}

export interface AssignRoleDTO {
  roleIdList: number[]
  userId: number | string
}
