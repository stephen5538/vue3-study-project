import type { AssignRoleDTO, Role, RoleAssign } from './type'
import request from '@/utils/userRequest'
import type { ResponseData, PageRecord } from '@/api/type'
enum API {
  GET_ROLE_BY_USER_ID = '/admin/acl/user/toAssign',
  DO_ASSIGN_ROLE = '/admin/acl/user/doAssignRole',
  PAGE = '/admin/acl/role',
  ADD = '/admin/acl/role/save',
  UPDATE = '/admin/acl/role/update',
  REMOVE = '/admin/acl/role/remove',
}

export const reqGetRoleByUserId = (userId: number | string) =>
  request.get<null, ResponseData<RoleAssign>>(
    API.GET_ROLE_BY_USER_ID + `/${userId}`,
  )

export const reqDoAssignRole = (data: AssignRoleDTO) =>
  request.post<RoleAssign, ResponseData<null>>(API.DO_ASSIGN_ROLE, data)

export const reqRoleList = (page: number, limit: number, roleName: string) =>
  request.get<null, ResponseData<PageRecord<Role>>>(
    API.PAGE + `/${page}/${limit}?roleName=${roleName}`,
  )

export const reqAddOrUpdateRole = (data: Role) => {
  if (data.id) {
    return request.put<Role, ResponseData<string>>(API.UPDATE, data)
  } else {
    return request.post<Role, ResponseData<string>>(API.ADD, data)
  }
}

export const reqRemoveRole = (id: number | string) =>
  request.delete<null, ResponseData<string>>(API.REMOVE + `/${id}`)
