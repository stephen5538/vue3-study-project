export interface Permission {
  id?: number | string
  createTime?: string
  updateTime?: string
  pid: number | string
  name: string
  code?: number | string
  toCode?: number | string
  type?: number | string
  status?: number | string
  level: number | string
  children?: Permissions
  select?: boolean
}

export type Permissions = Permission[]

export interface AssignPermissionDTO {
  roleId: number | string
  permissionIdList: number[]
}
