import type { Permissions, AssignPermissionDTO, Permission } from './type'
import request from '@/utils/userRequest'
import type { ResponseData } from '@/api/type'
enum API {
  ALL = '/admin/acl/permission',
  GET_ALL_BY_ROLE_ID = '/admin/acl/permission/toAssign',
  DO_ASSIGN_ACL = '/admin/acl/permission/doAssignAcl',
  ADD = '/admin/acl/permission/save',
  UPDATE = '/admin/acl/permission/update',
  REMOVE = '/admin/acl/permission/remove',
}

export const reqAllPermissionList = () =>
  request.get<null, ResponseData<Permissions>>(API.ALL)

export const reqGetAllPermissionByRoleId = (roleId: number | string) =>
  request.get<null, ResponseData<Permissions>>(
    API.GET_ALL_BY_ROLE_ID + '/' + roleId,
  )

export const reqDoAssignPermission = (data: AssignPermissionDTO) =>
  request.post<AssignPermissionDTO, ResponseData<null>>(API.DO_ASSIGN_ACL, data)

export const reqAddOrUpdatePermission = (data: Permission) => {
  if (data.id) {
    return request.put<Permission, ResponseData<string>>(API.UPDATE, data)
  } else {
    return request.post<Permission, ResponseData<string>>(API.ADD, data)
  }
}

export const reqRemovePermission = (id: number | string) =>
  request.delete<null, ResponseData<string>>(API.REMOVE + `/${id}`)
