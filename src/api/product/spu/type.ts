export interface Spu {
  id?: number | string
  spuName: string
  description: string
  // 品牌id
  tmId: number | string
  // 三级分类id
  category3Id: number | string
  // 销售属性列表
  spuSaleAttrList: SpuSaleAttrs | null
  // spu图片列表
  spuImageList: SpuImages | null
}

export type Spus = Spu[]
export type SpuImages = SpuImage[]
export type SpuSaleAttrs = SpuSaleAttr[]
export type SpuSaleAttrValues = SpuSaleAttrValue[]
export type SaleAttrs = SaleAttr[]

export interface SpuImage {
  id?: number | string
  imgName?: string
  imgUrl?: string
  spuId?: number | string
}

export interface SpuSaleAttr {
  id?: number | string
  baseSaleAttrId: number | string
  saleAttrName?: string
  spuId: number | string | undefined
  spuSaleAttrValueList: SpuSaleAttrValues
  spuSaleAttrIdAndSpuSaleAttrValueId?: string
}

export interface SpuSaleAttrValue {
  id?: number | string
  saleAttrName: string
  baseSaleAttrId: number | string
  saleAttrValueName: string
  spuId: number | string | undefined
  flag: boolean
}

export interface SaleAttr {
  id?: number | string
  name: string
}
