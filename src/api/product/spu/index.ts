import { PageRecord, ResponseData } from '@/api/type'
import type { Spu, SpuImages, SpuSaleAttrs, SaleAttrs } from './type'
import request from '@/utils/request'

enum API {
  PAGE_SPU_URL = '/admin/product/',
  ADD_SPU_URL = '/admin/product/saveSpuInfo',
  UPDATE_SPU_URL = '/admin/product/updateSpuInfo',
  GET_SPU_IMAGE_LIST = '/admin/product/spuImageList',
  GET_SPU_SALE_ATTR_LIST = '/admin/product/spuSaleAttrList',
  GET_ALL_SALE_ATTR_LIST = '/admin/product/baseSaleAttrList',
  DELETE_SPU_URL = '/admin/product/deleteSpu',
}

export const reqSpuList = (
  page: number,
  limit: number,
  category3Id: number | string,
) =>
  request.get<null, ResponseData<PageRecord<Spu>>>(
    API.PAGE_SPU_URL + `${page}/${limit}?category3Id=${category3Id}`,
  )

export const reqAddOrUpdateSpu = (data: Spu) => {
  if (data.id) {
    return request.post<Spu, ResponseData<null>>(API.UPDATE_SPU_URL, data)
  } else {
    return request.post<Spu, ResponseData<null>>(API.ADD_SPU_URL, data)
  }
}

export const reqSpuImageList = (spuId: number | string) =>
  request.get<null, ResponseData<SpuImages>>(
    API.GET_SPU_IMAGE_LIST + `/${spuId}`,
  )

export const reqSpuSaleAttrList = (spuId: number | string) =>
  request.get<null, ResponseData<SpuSaleAttrs>>(
    API.GET_SPU_SALE_ATTR_LIST + `/${spuId}`,
  )

export const reqAllSaleAttrList = () =>
  request.get<null, ResponseData<SaleAttrs>>(API.GET_ALL_SALE_ATTR_LIST)

export const reqDeleteSpu = (spuId: number | string) =>
  request.delete<null, ResponseData<null>>(API.DELETE_SPU_URL + `/${spuId}`)
