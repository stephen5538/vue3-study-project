export interface Attr {
  id?: number | string
  attrName: string
  attrValueList: AttrValueList
  categoryId: string | number
  categoryLevel: number
  attrIdAndAttrValueId?: string
}
export interface AttrValue {
  id?: number | string
  attrId?: number | string
  valueName: string
  flag?: boolean
}

export type Attrs = Attr[]
export type AttrValueList = AttrValue[]
