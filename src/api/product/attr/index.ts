import request from '@/utils/request'
import type { ResponseData } from '@/api/type'
import type { Attr, Attrs, AttrValueList } from './type'

enum API {
  QUERY_ATTR_URL = '/admin/product/attrInfoList',
  REMOVE_ATTR_URL = '/admin/product/deleteAttr',
  SAVE_ATTR_URL = '/admin/product/saveAttrInfo',
  QUERY_ATTR_VALUE_URL = '/admin/product/getAttrValueList',
}

export const reqQueryAttr = (
  category1Id: number | string,
  category2Id: number | string,
  category3Id: number | string,
) =>
  request.get<null, ResponseData<Attrs>>(
    API.QUERY_ATTR_URL + `/${category1Id}/${category2Id}/${category3Id}`,
  )

export const reqQueryAttrValue = (attrId: number | string) =>
  request.get<null, ResponseData<AttrValueList>>(
    API.QUERY_ATTR_VALUE_URL + `/${attrId}`,
  )

export const reqRemoveAttr = (attrId: number | string) =>
  request.delete<null, ResponseData<null>>(API.REMOVE_ATTR_URL + `/${attrId}`)

export const reqSaveOrUpdateAttr = (data: Attr) =>
  request.post<Attr, ResponseData<null>>(API.SAVE_ATTR_URL, data)
