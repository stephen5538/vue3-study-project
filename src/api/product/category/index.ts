import request from '@/utils/request'
import { Categories } from './type'
import { ResponseData } from '@/api/type'

enum API {
  GET_CATEGORY1_URL = '/admin/product/getCategory1',
  GET_CATEGORY2_URL = '/admin/product/getCategory2',
  GET_CATEGORY3_URL = '/admin/product/getCategory3',
}

export const reqGetCategory1 = () =>
  request.get<null, ResponseData<Categories>>(API.GET_CATEGORY1_URL)
export const reqGetCategory2 = (category1Id: number | string) =>
  request.get<null, ResponseData<Categories>>(
    API.GET_CATEGORY2_URL + `/${category1Id}`,
  )
export const reqGetCategory3 = (category2Id: number | string) =>
  request.get<null, ResponseData<Categories>>(
    API.GET_CATEGORY3_URL + `/${category2Id}`,
  )
