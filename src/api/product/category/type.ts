export interface Category {
  id?: number | string
  name: string
  category1Id?: number | string
  category2Id?: number | string
}

export type Categories = Category[]
