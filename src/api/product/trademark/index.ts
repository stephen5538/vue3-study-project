import request from '@/utils/request'
import { ResponseData, PageRecord } from '@/api/type'
import type { Trademark, Trademarks } from './type'

enum API {
  TRADEMARK_LIST_URL = '/admin/product/baseTrademark/getTrademarkList',
  //品牌分页接口
  TRADEMARK_URL = '/admin/product/baseTrademark',
  TRADEMARK_ADD_URL = '/admin/product/baseTrademark/save',
  TRADEMARK_UPDATE_URL = '/admin/product/baseTrademark/update',
  TRADEMARK_DELETE_URL = '/admin/product/baseTrademark/remove',
}

export const reqTrademarkListAll = () =>
  request.get<null, ResponseData<Trademarks>>(API.TRADEMARK_LIST_URL)

export const reqTrademarkList = (page: number, limit: number) =>
  request.get<null, ResponseData<PageRecord<Trademark>>>(
    API.TRADEMARK_URL + `/${page}/${limit}`,
  )

export const reqAddOrUpdateTrademark = (data: Trademark) => {
  //如果有id 则更新否则新增
  if (data.id) {
    return request.put<Trademark, ResponseData<null>>(
      API.TRADEMARK_UPDATE_URL,
      data,
    )
  } else {
    return request.post<Trademark, ResponseData<null>>(
      API.TRADEMARK_ADD_URL,
      data,
    )
  }
}

export const reqDeleteTrademark = (id: number) =>
  request.delete<null, ResponseData<null>>(API.TRADEMARK_DELETE_URL + `/${id}`)
