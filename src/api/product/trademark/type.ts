export interface Trademark {
  id?: number
  logoUrl: string
  tmName: string
}

export type Trademarks = Trademark[]
