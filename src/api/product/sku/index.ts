import request from '@/utils/request'
import type { PageRecord, ResponseData } from '@/api/type'
import type { SkuInfo } from './type'

enum API {
  SAVE_SKU_URL = '/admin/product/saveSkuInfo',
  UPDATE_SKU_URL = '/admin/product/updateSkuInfo',
  FIND_SKU_URL = '/admin/product/findBySpuId',
  PAGE_SKU_URL = '/admin/product/list',
  ON_SALE_SKU_URL = '/admin/product/onSale',
  CANCEL_SALE_SKU_URL = '/admin/product/cancelSale',
  GET_SALE_SKU_URL = '/admin/product/getSkuInfo',
  DEL_SKU_URL = '/admin/product/deleteSku',
}

export const reqSaveSkuInfo = (data: SkuInfo) =>
  request.post<SkuInfo, ResponseData<null>>(API.SAVE_SKU_URL, data)

export const reqUpdateSkuInfo = (data: SkuInfo) =>
  request.post<SkuInfo, ResponseData<null>>(API.UPDATE_SKU_URL, data)

export const reqFindSkuInfoBySpuId = (spuId: number | string) =>
  request.get<null, ResponseData<SkuInfo[]>>(API.FIND_SKU_URL + `/${spuId}`)

export const reqSkuList = (page: number, limit: number) =>
  request.get<null, ResponseData<PageRecord<SkuInfo>>>(
    API.PAGE_SKU_URL + `/${page}/${limit}`,
  )

export const reqOnSaleSku = (skuId: number | string) =>
  request.get<null, ResponseData<null>>(API.ON_SALE_SKU_URL + `/${skuId}`)

export const reqCancelSaleSku = (skuId: number | string) =>
  request.get<null, ResponseData<null>>(API.CANCEL_SALE_SKU_URL + `/${skuId}`)

export const reqGetSkuInfo = (skuId: number | string) =>
  request.get<null, ResponseData<SkuInfo>>(API.GET_SALE_SKU_URL + `/${skuId}`)

export const reqDelSku = (skuId: number | string) =>
  request.delete<null, ResponseData<string>>(API.DEL_SKU_URL + `/${skuId}`)
