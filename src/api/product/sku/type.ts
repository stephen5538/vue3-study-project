export interface SkuInfo {
  id?: number | string
  skuName: string
  skuDesc: string
  category3Id: number | string
  spuId: number | string
  tmId: number | string
  skuAttrValueList: SkuAttrValues
  skuSaleAttrValueList: SkuSaleAttrValues
  skuDefaultImg?: string
  price: number
  weight: number
  isSale?: number | string
  skuImageList?: SkuImages
}
export type SkuAttrValues = SkuAttrValue[]
export type SkuSaleAttrValues = SkuSaleAttrValue[]
export type SkuImages = SkuImage[]

export interface SkuAttrValue {
  id?: number | string
  attrId: number | string //平台属性ID
  valueId: number | string //平台属性值的ID
  attrName?: string
  valueName?: string
}
export interface SkuImage {
  id?: number | string
  imgName: string
  imgUrl: string
  spuImgId?: number | string
  isDefault: string
}

export interface SkuSaleAttrValue {
  id?: number | string
  saleAttrId: number | string
  saleAttrValueId: number | string
  saleAttrName?: string
  saleAttrValueName?: string
}
