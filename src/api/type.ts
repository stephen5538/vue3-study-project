//定义公共接口返回类型
export interface ResponseData<T> {
  code: number
  message: string
  ok: boolean
  data: T
}

export interface PageRecord<T> {
  records: T[]
  total: number
  size: number
  current: number
  pages: number
}
